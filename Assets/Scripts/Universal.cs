﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum DownloadMode
{
    Online,
    Offline,
    Hybrid
}

public enum AspectRatios
{
    _3x4,
    _2x3,
    _10x16,
    Default
}

[Serializable]
public class QuestionFormat
{
    /*public string Question;
    public string Explanation;
    public string Image;
    public bool isToF;
    public bool isAnswerLink;
    public bool isAnswer;
    public bool isDrawing;
    public bool isRecording;
    public bool ToFAnswer;
    public AnswerFormat[] Answers = new AnswerFormat[0];*/

    public string Index;
    public string TextName;
    public int TextIndex;
    public bool IsInstruction;
    public string ExplainationImage;
    public string ExplainationText;
    public string ExplainationAudio;
    public bool IsTrial;
    public string Question;
    public string QuestionAudio;
    public string QuestionAudioSetting;
    public string QuestionImage;
    public string QuestionImageSizeType;
    public bool IsChoicewithWord;
    public bool IsChoicewithImages;
    public bool IsDrawing;
    public bool IsRecording;
    public bool IsTellImage;
    public bool IsMarking;
    public string QuestionSliced;
    public string RecordingLimited;
    public string TimeLimited;
    public ChoiceDetailsFormat[] ChoiceDetails = new ChoiceDetailsFormat[0];
}

[Serializable]
public class AnswerFormat
{
    public string Text;
    public bool Correct;
}

[Serializable]
public class ChoiceDetailsFormat
{
    public string choice;
    public string correct;
}

public struct QuestionsContainer
{
    public List<QuestionFormat> Questions;

    public QuestionsContainer(List<QuestionFormat> data)
    {
        Questions = data;
    }
}

public class UserResult
{
    public user[] Users;
}

[Serializable]
public class user {
    public int id;
    public string username;
    public string classname;
    public string gender;
    public string stNum;
    public string birth;
    public string hasFinished;
    public int totalSeconds;
}

public struct FrogSettingContainer
{
    public List<frogSetting> frogSettings;
    public bool Ordered;

    public FrogSettingContainer(List<frogSetting> data, bool ordered)
    {
        frogSettings = data;
        Ordered = ordered;
    }
}

[Serializable]
public class frogSetting {
    public string Index;
    public bool IsTrial;
    public int Level;
    public int MaxNum;
    public int PassNum;
    public int TestIndex;
}

public struct VSSettingContainer
{
    public List<VSSetting> vsSettings;

    public VSSettingContainer(List<VSSetting> data)
    {
        vsSettings = data;
    }
}

[Serializable]
public class VSSetting {
    public string Index;
    public int[] Rows;
    public string Type;
    public int[] Nums;
    public bool IsTrail;
    public int TestIndex;
}

[Serializable]
public class CategoryFormat
{
    public bool Enabled;
    public string CategoryName;
    public Sprite CategoryImage;
    public DownloadMode Mode;
    public string OnlinePath;
    public TextAsset OfflineFile;
    public bool ShuffleQuestions;
    public bool ShuffleAnswers;
    public bool LimitQuestions;
    public int QuestionLimit;
    public bool CustomTimerAmount;
    public int TimerAmount;
    public bool CustomLivesAmount;
    public int LivesCount;
    public LanguageList Language;
    public string HighscorePref;
    public TestTemplate SpecialTemplate;
    public float Time2Finishe;
    public int TestIndex;
    public bool CanExitHalf;



    public CategoryFormat(LanguageList Lang = 0)
    {
        CategoryName = "New";
        OnlinePath = HighscorePref = string.Empty;
        OfflineFile = null;
        Mode = DownloadMode.Offline;
        Language = Lang;
        ShuffleQuestions = ShuffleAnswers = Enabled = true;
        LimitQuestions = CustomTimerAmount = CustomLivesAmount = false;
        QuestionLimit = TimerAmount = LivesCount = 0;
        CategoryImage = null;
        SpecialTemplate = TestTemplate.Common;
        CanExitHalf = false;
    }

    public enum TestTemplate
    {
        Common,
        FrogMap,
        AutoVisualSearch,
        PPTV

    }

    public CategoryFormat(CategoryFormat format)
    {
        Enabled = format.Enabled;
        CategoryName = format.CategoryName;
        CategoryImage = format.CategoryImage;
        Mode = format.Mode;
        OnlinePath = format.OnlinePath;
        OfflineFile = format.OfflineFile;
        ShuffleQuestions = format.ShuffleQuestions;
        ShuffleAnswers = format.ShuffleAnswers;
        LimitQuestions = format.LimitQuestions;
        QuestionLimit = format.QuestionLimit;
        CustomTimerAmount = format.CustomTimerAmount;
        TimerAmount = format.TimerAmount;
        CustomLivesAmount = format.CustomLivesAmount;
        LivesCount = format.LivesCount;
        Language = format.Language;
        HighscorePref = format.HighscorePref;
    }
}

public struct BackupContainer
{
    public List<CategoryFormat> Categories;

    public BackupContainer(List<CategoryFormat> data)
    {
        Categories = data;
    }
}

[Serializable]
public class LocalizationFormat
{
    public string Key;
    public string Value;

    public LocalizationFormat(string key, string value)
    {
        Key = key;
        Value = value;
    }
}

public struct LocalizationContainer
{
    public List<LocalizationFormat> Locale;

    public LocalizationContainer(List<LocalizationFormat> data)
    {
        Locale = data;
    }
}

[Serializable]
public class User_Record{
    public int questionIndex;

}

public struct UserRecordContainer {
    public List<User_Record> Records;

    public UserRecordContainer(List<User_Record> data)
    {
        Records = data;
    }
}

[Serializable]
public class Class_Info {
    public int id;
    public string classname;
    public string remark;
}

public struct ClassInfoContainer {
    public List<Class_Info> ClassInfo;

    public ClassInfoContainer(List<Class_Info> data) {
        ClassInfo = data;
    }

}

