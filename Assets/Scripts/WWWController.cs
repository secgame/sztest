﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class WWWController : MonoBehaviour
{
    public Sprite image;
    //public string imageURL = "https://secphillab.com/SZTest/Upload.php";
     string imageURL = "http://loxiaomjng.99hl.cn/SZTest/Upload.php";

    public string location;
    //public string videoURL = "https://secphillab.com/SZTest/Upload.php";
     string videoURL = "http://loxiaomjng.99hl.cn/SZTest/Upload.php";

    public string filename;

    int userIndex;
    int questionIndex;
    string answerStr;
    string startTimeStr;
    string endTimeStr;
    //string answerURL = "https://secphillab.com/SZTest/postData.php";
    string answerURL = "http://loxiaomjng.99hl.cn/SZTest/postData.php";


    /// <summary>
    /// 用戶信息
    /// </summary>
    public string username;
    public int classname;
    public string stNum;
    public string sex;
    public string birth;
    public string hasfinished;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UploadMedia(int type,string file_name, Sprite spr = null, string loca = null) {
        filename = file_name;
        if (type == 1)
        {
            image = spr;
            StartCoroutine(UploadImage());
        }

        else {
            location = loca;
            StartCoroutine(UploadVideo());
        }
            
    }

    public void SaveToDatabase(int userId, int questionId, string answer,string time_start, string time_end) {
        userIndex = userId;
        questionIndex = questionId;
        answerStr = answer;
        startTimeStr = time_start;
        endTimeStr = time_end;

        StartCoroutine(UploadAnswer());
    }


    //上傳圖片到指定的文件夾
    private IEnumerator UploadImage() {
        byte[] bytes = SpriteTBytes(image); //獲取圖片數據
        WWWForm form = new WWWForm();//創建提交數據表單
        form.AddBinaryData("file", bytes, filename, "image/png");//字段名，文件數據，文件名，文件類型
        using (UnityWebRequest www = UnityWebRequest.Post(imageURL, form)) { 
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
           
    }

    //獲取圖片的二進制數據
    public byte[] SpriteTBytes(Sprite sp) {
        Texture2D t = sp.texture;
        byte[] bytes = t.EncodeToJPG();
        return bytes;
    }

    private IEnumerator UploadVideo() {

        byte[] bytes = File.ReadAllBytes(location);//文件暫存的地址
        WWWForm form = new WWWForm();
        form.AddBinaryData("file", bytes, filename);

        using (UnityWebRequest www = UnityWebRequest.Post(videoURL, form)) {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else {
                Debug.Log(videoURL);
                Debug.Log("文件已經上傳："+ www.downloadHandler.text.ToString());
            }

        }
    }

    private IEnumerator UploadAnswer() {
        WWWForm form = new WWWForm();
        form.AddField("method", "insert");
        form.AddField("table", "tb_record");
        form.AddField("userIndex", userIndex);
        form.AddField("questionIndex", questionIndex);
        form.AddField("answer", answerStr);
        form.AddField("time_start", startTimeStr);
        form.AddField("time_end", endTimeStr);

        using (UnityWebRequest www = UnityWebRequest.Post(answerURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("數據保存成功");
            }

        }


    }

    public void FindName(int t_class, string t_stNum) {
        classname = t_class;
        stNum = t_stNum;
        Debug.Log("classname:" + classname + "; stnum:" + stNum);
        StartCoroutine(SelectStNum());
    }

    IEnumerator SelectStNum() {
        WWWForm form = new WWWForm();
        form.AddField("method", "getAll");
        form.AddField("table", "tb_user");
        form.AddField("classname", classname);
        form.AddField("stnum", stNum);

        using (UnityWebRequest www = UnityWebRequest.Post(answerURL, form)) {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                gameObject.GetComponent<LoginController>().ConnectedFail();
            }
            else {
                //Debug.Log(www.downloadHandler.text.ToString());
               
                if (www.downloadHandler.text.ToString().Trim()=="" || www.downloadHandler.text.ToString().Trim() == "[]")
                {
                    gameObject.GetComponent<LoginController>().FindNamerFail();
                }
                else {
                    UserResult Result = JsonUtility.FromJson<UserResult>("{\"Users\":" + www.downloadHandler.text.ToString() + "}");
                    user user = Result.Users[0];
                    gameObject.GetComponent<LoginController>().FindNameSuccess(user);
                }
                
            }
        }
    }

    public IEnumerator SelectUserRecord(int userId, int TextIndex) {
        WWWForm form = new WWWForm();
        form.AddField("method", "sql");
        form.AddField("table", "tb_record");
        form.AddField("sql", "select DISTINCT questionIndex from tb_record where userIndex=" + userId+ " and Cast(questionIndex As char(10)) like '" + TextIndex + "%'");
        Debug.Log("sql:" + "select DISTINCT questionIndex from tb_record where userIndex=" + userId + " and Cast(questionIndex As char(10)) like '" + TextIndex + "%'");

        using (UnityWebRequest www = UnityWebRequest.Post(answerURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.downloadHandler.text.ToString() == "false" || www.downloadHandler.text.ToString() == "")
                {
                    Debug.Log("没有数据");
                    
                }
                else
                {
                    Debug.Log("{\"Records\":" + www.downloadHandler.text.ToString() + "}");
                    UserRecordContainer Result = JsonUtility.FromJson<UserRecordContainer>("{\"Records\":" + www.downloadHandler.text.ToString() + "}");
                    Debug.Log("Result.Records:" + Result.Records);
                    Controller.instance.SetUserRecord(Result.Records);

                }
                Debug.Log("獲取數據成功");
            }
        }
    }

    public IEnumerator GetClassInfo() {
        WWWForm form = new WWWForm();
        form.AddField("method", "getAll");
        form.AddField("table", "tb_class");
        using (UnityWebRequest www = UnityWebRequest.Post(answerURL,form)) {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);               
            }
            else {
                ClassInfoContainer Result = JsonUtility.FromJson<ClassInfoContainer>("{\"ClassInfo\":" + www.downloadHandler.text.ToString() + "}");
                gameObject.GetComponent<LoginController>().SetClassInfo(Result.ClassInfo);
            }

        }
    }

    /// <summary>
    /// 尋找用戶
    /// </summary>
    /// <returns></returns>
   /* private IEnumerator SelectUser()
    {
        WWWForm form = new WWWForm();
        form.AddField("method", "get");
        form.AddField("table", "tb_user");
        form.AddField("username", username);
        Debug.Log("username:"+ username);

        using (UnityWebRequest www = UnityWebRequest.Post(answerURL, form)) {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.downloadHandler.text.ToString() == "false" || www.downloadHandler.text.ToString() == "")
                {
                    Debug.Log("沒有該學生。");
                    gameObject.GetComponent<LoginController>().RegisterFail();
                }
                else
                {
                    Debug.Log(www.downloadHandler.text.ToString());
                    user user = JsonUtility.FromJson<user>(www.downloadHandler.text.ToString());
                    string scolarId = user.id.ToString();
                    string hasFinished = user.hasFinished;
                    PlayerPrefs.SetString("scolarId", scolarId);
                    PlayerPrefs.SetString("hasFinished", hasFinished);
                    gameObject.GetComponent<LoginController>().RegisterSuccess();
                    StartCoroutine(UpdateUser());

                }
                Debug.Log("獲取數據成功");
            }
        }


    }*/

    public IEnumerator UpdateUser()
    {
        WWWForm form = new WWWForm();
         form.AddField("method", "update");
         form.AddField("table", "tb_user");
         form.AddField("id", Controller.instance.userId);
         form.AddField("hasFinished", Controller.instance.hasFinished);
        using (UnityWebRequest www = UnityWebRequest.Post(answerURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
                
        }
    }

   /* public void UpdateInformation(string t_name, string t_class, string t_sex, string t_stuNum, string t_birth) {
        username = t_name;
        classname = t_class;
        sex = t_sex;
        stNum = t_stuNum;
        birth = t_birth;
        StartCoroutine(SelectUser());
    }*/


}
