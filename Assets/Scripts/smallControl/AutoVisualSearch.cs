﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AutoVisualSearch : MonoBehaviour
{
    public GameObject VSItemParent;
    public GameObject imageTempate;
    public Button Continue;
    public Text AllInstruction;
    public Text FeedBack;
    public Text SubInstruction;
    public Image img_target;
    Dictionary<string, int> folders = new Dictionary<string, int>();
    public VSSetting[] VSSettings;
    public AudioClip[] clips;//0-> 总介绍；1->回答正确；2->回答错误；3->白色的横线；4->白色的竖线；5->红色的横线;6->红色的竖线；7->绿色的横线；8->绿色的竖线
    private int subInstructionIndex;

    int VSSettingIndex;
    VSSetting currentSetting;

    bool inverseOrder = true;
    int[] questionSetting;
    bool[] randomsetting;
    int currentSubIndex;
    int targetPosIndex;
    int correctAns;
    int ContinueState; //0->AllIntroduction，1->subIntroduction, 2->Question

    System.Random gen;
    string TargetFolder = "";
    string TargetImage = "";
    string currentVSType;
    bool isTrail;

    string startTimeStamp;
    string endTimeStamp;
    int AnsIndex = -1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetVSsettings(VSSetting[] settings) {
        VSSettings = settings;

        StartCoroutine(PrepareGame());
    }

    IEnumerator PrepareGame() {
        //初始化
        //questionSetting = new string[,] { { "3", "Feature" }, { "4", "Feature" }, { "6", "Feature" }, { "3", "Feature" }, { "4", "Feature" }, { "6", "Feature" } };

        yield return new WaitForSeconds(2f);
        Controller.instance.QuestionNumText.color = Color.white;
        VSSettingIndex = 0;
        currentSubIndex = 0;
        VSItemParent.SetActive(false);
        FeedBack.gameObject.SetActive(false);
        SubInstruction.gameObject.SetActive(false);

        Continue.gameObject.SetActive(true);
        Continue.transform.GetChild(0).GetComponent<Text>().text = "进入测试";
        AllInstruction.gameObject.SetActive(true);

        folders.Clear();
        folders.Add("Feature", 2);
        folders.Add("Conjuction", 4);

        gen = new System.Random();
        ContinueState = 0;
        correctAns = -1;

        //播放语音
        Controller.instance.PlaySound(clips[0]);
        StartCoroutine(AudioPlayFinished(clips[0].length));
    }

    private IEnumerator AudioPlayFinished(float time)
    {
        Debug.Log("audiotime:" + time);
        Continue.interactable = false;
        yield return new WaitForSeconds(time);
        Debug.Log("audiostoped");
        Continue.interactable = true ;


    }

        // Update is called once per frame
        void Update()
    {

    }

    public List<T> ListRandom<T>(List<T> sources)
    {
        System.Random rd = new System.Random();
        int index = 0;
        T temp;
        for (int i = 0; i < sources.Count; i++)
        {
            index = rd.Next(0, sources.Count - 1);
            if (index != i)
            {
                temp = sources[i];
                sources[i] = sources[index];
                sources[index] = temp;
            }
        }
        return sources;
    }


    public void LoadVSQuestion(bool newStart = false) {

        //先获得当前的setting
        if (newStart) {         
            currentSetting = VSSettings[VSSettingIndex];
            ArrayList questionArrList = new ArrayList();
            currentVSType = currentSetting.Type;
            isTrail = currentSetting.IsTrail;
            Debug.Log("currentSetting.IsTrail:" + currentSetting.IsTrail + ";VSSettingIndex:"+ VSSettingIndex);
            ArrayList randomArrList = new ArrayList();
            for (int i = 0; i < currentSetting.Rows.Length; i++) {
                List<bool> trueorfalse = new List<bool>();
                for (int j = 0; j < currentSetting.Nums[i]; j++) {
                    questionArrList.Add(currentSetting.Rows[i]);
                    if (j < currentSetting.Nums[i] / 2) {
                        trueorfalse.Add(false);
                    }
                    else {
                        trueorfalse.Add(true);
                    }
                                     
                }
                //生成一半的0，一半的1然后乱序
                randomArrList.AddRange(ListRandom(trueorfalse));

            }
            questionSetting = (int[])questionArrList.ToArray(typeof(int));
            randomsetting = (bool[])randomArrList.ToArray(typeof(bool));
            Debug.Log("questionSetting:" + string.Join(",", questionSetting));
            Debug.Log("randomsetting:" + string.Join(",", randomsetting));
            VSSettingIndex++;
        }

        if (isTrail)
        {
            Controller.instance.QuestionNumText.text = "答题进度：练习阶段";
        }
        else {
            Controller.instance.QuestionNumText.text = "答题进度："+ currentSubIndex + "/"+questionSetting.Length;
        }

        //獲得行列數
        if (currentSubIndex == questionSetting.Length-1) {
            ContinueState = 3;
        }

        //清空當前的內容
        int childCount = VSItemParent.transform.childCount;
        for (int i = childCount-1; i >=0; i--)
        {
            Destroy(VSItemParent.transform.GetChild(i).gameObject);
        }


        int row = questionSetting[currentSubIndex];
        Debug.Log("currentSubIndex:"+ currentSubIndex);
        int folderCount = folders[currentVSType];
        int num = row * row;    
        bool hasTarget = randomsetting[currentSubIndex];

        VSItemParent.GetComponent<GridLayoutGroup>().constraintCount = row;

        if (newStart) {
            //TargetFolder = "T" + gen.Next(1, folderCount+1);
            //TargetImage = TargetFolder + "_" + gen.Next(1, 4);
            TargetFolder = "T1";
            TargetImage = TargetFolder + "_2";
            img_target.sprite = Resources.Load<Sprite>("images/"+ currentVSType + "/" + TargetFolder + "/" + TargetImage);

            switch (currentVSType) {
                case "Feature":
                    switch (TargetFolder) {
                        case "T1":
                            subInstructionIndex = 3;
                            break;
                        case "T2":
                            subInstructionIndex = 4;
                            break;
                    }
                    break;
                case "Conjuction":
                    switch (TargetFolder)
                    {
                        case "T1":
                            subInstructionIndex = 5;
                            break;
                        case "T2":
                            subInstructionIndex = 7;
                            break;
                        case "T3":
                            subInstructionIndex = 8;
                            break;
                        case "T4":
                            subInstructionIndex = 6;
                            break;
                    }
                    break;
            }
        }

        //有目標的話就取代掉一個
        targetPosIndex = -1;
        if (hasTarget)
        {
            targetPosIndex = gen.Next(0, num);
        }

        //先隨機把空格都塞滿，避開TargetFolder
        for (int i = 0;i < num;i++) {
            //先隨機獲得圖片
            string tmpFolder = "";
            do{
                tmpFolder = "T" + gen.Next(1, folderCount+1);
            }while (tmpFolder == TargetFolder ) ;

            string tmpImage  = tmpFolder + "_" + gen.Next(1, 4);

            GameObject go = Instantiate(imageTempate);
            if (targetPosIndex == i)
            {
                go.GetComponent<Image>().sprite = Resources.Load<Sprite>("images/" + currentVSType + '/' + TargetFolder + '/' + TargetImage);
            }
            else {
                go.GetComponent<Image>().sprite = Resources.Load<Sprite>("images/" + currentVSType + '/' + tmpFolder + '/' + tmpImage);
            }
            
            go.transform.SetParent(VSItemParent.transform);
            go.transform.localScale = Vector3.one;
        }
        
    }

    public void OnclickContinue() {
        Controller.instance.AudioPlayer.Stop();
        switch (ContinueState)
        {
            case 0:
                LoadVSQuestion(true);
                ShowSubIntroduction();
                ContinueState = 1;
                break;
            case 1:
                StartCoroutine(ShowCross());
                ContinueState = 2;
                break;
            case 2:               
                Debug.Log("AnsIndex:" + AnsIndex);               
                Debug.Log("isTrail:"+ isTrail);
                correctAns = targetPosIndex;
                endTimeStamp = Controller.GetTimeStamp();
                ToggleEnableAns(false);               
                if (isTrail)
                {
                    StartCoroutine(ShowJudge());
                }
                else {
                    //保存答案
                    SaveAns(AnsIndex);
                    StartCoroutine(ShowCross());                  
                   
                }
               
                currentSubIndex++;
                LoadVSQuestion();

                AnsIndex = -1;
                break;
            case 3:
                Debug.Log("AnsIndex:" + AnsIndex);
                endTimeStamp = Controller.GetTimeStamp();
                correctAns = targetPosIndex;
                if (!isTrail)
                {
                    //保存答案
                   SaveAns(AnsIndex);
                }
               
                StartCoroutine(ShowFinished());
                AnsIndex = -1;
                break;
        }
    }

    IEnumerator ShowJudge() {
        if (AnsIndex == correctAns)
        {
            VSItemParent.SetActive(false);
            Continue.gameObject.SetActive(false);
            FeedBack.text = "回答正确！";
            FeedBack.gameObject.SetActive(true);
            //播放语音
            Controller.instance.PlaySound(clips[1]);
        }
        else {
            VSItemParent.SetActive(false);
            Continue.gameObject.SetActive(false);
            FeedBack.text = "回答错误！";
            FeedBack.gameObject.SetActive(true);
            //播放语音
            Controller.instance.PlaySound(clips[2]);
        }

        yield return new WaitForSeconds(2f);
        StartCoroutine(ShowCross());

            
    }



    /*IEnumerator ShowSubInstruction() {

    }*/

    IEnumerator ShowCross() {
        VSItemParent.SetActive(false);

        Continue.gameObject.SetActive(false);
        AllInstruction.gameObject.SetActive(false);
        SubInstruction.gameObject.SetActive(false);

        FeedBack.gameObject.SetActive(false);
        FeedBack.text = "+";
        FeedBack.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.3f);

        StartCoroutine(ShowBlank());

        StopCoroutine(ShowCross());
    }


    IEnumerator ShowBlank() {
        VSItemParent.SetActive(false);

        Continue.gameObject.SetActive(false);
        AllInstruction.gameObject.SetActive(false);
        SubInstruction.gameObject.SetActive(false);

        FeedBack.gameObject.SetActive(false);
        FeedBack.text = "";
        yield return new WaitForSeconds(0.3f);
        ShowQuestion();
        StopCoroutine(ShowBlank());
    }

    void ShowQuestion() {
        VSItemParent.SetActive(true);
        Continue.transform.GetChild(0).GetComponent<Text>().text = "无目标";
        ToggleEnableAns(true);
        Continue.gameObject.SetActive(true);
        startTimeStamp = Controller.GetTimeStamp();
    }

    void ShowSubIntroduction()
    {
        VSItemParent.SetActive(false);
        Continue.transform.GetChild(0).GetComponent<Text>().text = "开始测试";
        Continue.gameObject.SetActive(true);
        AllInstruction.gameObject.SetActive(false);
        if (isTrail)
        {
            SubInstruction.text = "我们先来测试一下。这次要找的【目标】是 \n \n \n 如果有【目标】，请在图片上把它点出来\n 如果无【目标】，请点击【无目标】按钮";
        }
        else {
            SubInstruction.text = "现在是正式测试。这次要找的【目标】是 \n \n \n 如果有【目标】，请在图片上把它点出来\n 如果无【目标】，请点击【无目标】按钮";
        }
        SubInstruction.gameObject.SetActive(true);
        //播放语音

        Controller.instance.PlaySound(clips[subInstructionIndex]);
        StartCoroutine( AudioPlayFinished(clips[subInstructionIndex].length));
        Controller.instance.ExplanationAudio.SetActive(true);

        FeedBack.gameObject.SetActive(false);
    }

    IEnumerator ShowFinished()
    {
        if (isTrail)
        {
            if (AnsIndex == correctAns)
            {
                VSItemParent.SetActive(false);
                Continue.gameObject.SetActive(false);
                FeedBack.text = "回答正确！";
                FeedBack.gameObject.SetActive(true);
                //播放语音
                Controller.instance.ExplanationAudio.SetActive(true);
                Controller.instance.PlaySound(clips[1]);

            }
            else
            {
                VSItemParent.SetActive(false);
                Continue.gameObject.SetActive(false);
                FeedBack.text = "回答错误！";
                FeedBack.gameObject.SetActive(true);
                //播放语音
                Controller.instance.ExplanationAudio.SetActive(true);
                Controller.instance.PlaySound(clips[2]);

            }
        }
       
        yield return new WaitForSeconds(2f);
        if (VSSettingIndex < VSSettings.Length)
        {
            currentSubIndex = 0;
            VSItemParent.SetActive(false);
            FeedBack.gameObject.SetActive(false);
            SubInstruction.gameObject.SetActive(false);

            Continue.gameObject.SetActive(true);
            AllInstruction.gameObject.SetActive(true);
            //播放语音
            Controller.instance.ExplanationAudio.SetActive(true);
            Controller.instance.PlaySound(clips[0]);
            StartCoroutine( AudioPlayFinished(clips[0].length));

            ToggleEnableAns(true);

            ContinueState = 0;
            OnclickContinue();

        }
        else {
            VSItemParent.SetActive(false);

            Continue.gameObject.SetActive(false);
            AllInstruction.gameObject.SetActive(false);
            SubInstruction.gameObject.SetActive(false);

            FeedBack.gameObject.SetActive(false);
            FeedBack.text = "测试完成！";
            FeedBack.gameObject.SetActive(true);

            Invoke("Gameover", 1.0f);
        }

    }

    private void Gameover() {
        string str = Controller.instance.hasFinished;
        if (str=="")
        {
            str = currentSetting.TestIndex.ToString();
        }
        else
        {
            str = str+ "," + currentSetting.TestIndex.ToString();
        }

        PlayerPrefs.SetString("hasFinished", str);
        Controller.instance.hasFinished = str;
        //保存到數據庫
        StartCoroutine(Controller.instance.myWWW.UpdateUser());
        Controller.instance.GameOver();
    }

    public void ToggleEnableAns(bool enableClick) {
            foreach (Transform child in VSItemParent.transform)
            {
                child.gameObject.GetComponent<Button>().interactable = enableClick;
            }

            Continue.GetComponent<Button>().interactable = enableClick;     
    }


    public void SaveAns(int ansIndex) {

       //StartCoroutine( Controller.instance.CaptureByUI(VSItemParent.transform.GetComponent<RectTransform>()));
        //先把图片上传到数据库
        //Sprite spr = Resources.Load("Resources/tempImg/tempCapture.png") as Sprite;
       // string filename = currentSetting.Index.ToString() + "_" + Controller.instance.userId.ToString()+"_"+ currentSubIndex + "_" + Controller.GetTimeStamp() + ".png";
        //Controller.instance.myWWW.UploadMedia(1, filename, spr);
        //把数据上传到数据库
        Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, int.Parse(currentSetting.Index), "correntAns:"+ correctAns + "; chooseAns:"+ ansIndex+";questionNo:"+ currentSubIndex+";level:"+questionSetting[currentSubIndex], startTimeStamp, endTimeStamp);    
        //OnclickContinue();
    }

    public void SetAns(int ansIndex) {
            AnsIndex = ansIndex;
            OnclickContinue();
    }
 

}
