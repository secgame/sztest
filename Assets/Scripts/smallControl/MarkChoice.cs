﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MarkChoice : MonoBehaviour
{
    public int rowNum;
    public int columnNum;
    // Start is called before the first frame update

    //計算鼠標點擊位置，對應的像素位置
    public Transform textureOrigin;
    public Transform textureUPEnd;

    public BoxCollider boxCollider;

    //存儲點擊的圖片的texture2D getpixel() 使用
    private Texture2D clickTexture2D;

    //存儲鼠標點擊位置的像素值
    private Color testColor = Color.red;

    //存儲計算出來的像素點的位置
    private Vector2 colorPos;

    //存儲圖片定位點的屏幕坐標
    private Vector3 textureOriginScreenPosition;
    private Vector3 textureEndUPScreenPosition;

    public Image targetImage;
    public GameObject circle;
    public GameObject circles;
    public Button btn_submit;
    public Text targetText;
    public bool enableMark;

    public ArrayList MarkPosList;
    public ArrayList MarkTimeList;
    public ArrayList MarkIndexList;


    
    void Start()
    {
        enableMark = false;
        
    }

    private void HitColorChooseImage(RaycastHit hit) {
        Debug.Log("hit");
        if (hit.collider.name == "targetImage") {
            clickTexture2D = hit.collider.gameObject.GetComponent<Image>().sprite.texture;
            CaculateVector();
            //創建一個circle，放位置
           GameObject newCircle =   Instantiate(circle);
            newCircle.transform.SetParent(circles.transform);
            newCircle.transform.localPosition = new Vector2(colorPos.x, colorPos.y);
            newCircle.transform.localScale = Vector3.one;


        }
    }

    private void CaculateVector() {
        colorPos.x = (Input.mousePosition.x - textureOriginScreenPosition.x) / (textureEndUPScreenPosition.x - textureOriginScreenPosition.x) * clickTexture2D.width- clickTexture2D.width/2.0f;
        //colorPos.x = colorPos.x * 1.1f;
        colorPos.y = (Input.mousePosition.y - textureOriginScreenPosition.y) / (textureEndUPScreenPosition.y - textureOriginScreenPosition.y) * clickTexture2D.height- clickTexture2D.height / 2.0f;
        //colorPos.y = colorPos.y * 1.1f;
        //Debug.Log("("+ colorPos.x + "," + colorPos.y + ")");

        //保存位置和時間
        MarkPosList.Add(new Vector2(colorPos.x, colorPos.y));
        MarkTimeList.Add(Controller.GetTimeStamp());
        MarkIndexList.Add(GetMarkIndexFromPos(new Vector2(colorPos.x, colorPos.y)));
    }

    private int GetMarkIndexFromPos(Vector2 pos) {
        int index = -1;
        float cellwidth = (float)clickTexture2D.width / columnNum;
        float cellheight = (float)clickTexture2D.height / rowNum;
        int indexinrow = (int)((pos.x + clickTexture2D.width / 2) / cellwidth)+1;
        int indexincolumn = rowNum-(int)((pos.y + clickTexture2D.height / 2) / cellheight)-1;
        //Debug.Log("indexinrow:" + indexinrow + "; indexincolumn:" + indexincolumn);
        index = indexincolumn * columnNum + indexinrow;
        //Debug.Log("index:"+ index);
        return index;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && enableMark)
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                HitColorChooseImage(hit);
            }
        }
    }

   public void PrepareMarkChoice(int imagerow, int imagecolumn, string imagelink, string question) {
        Debug.Log("PrepareMarkChoice");
        //去掉circle
        rowNum = imagerow;
        columnNum = imagecolumn;
        targetImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/" + imagelink.Split('.')[0]);
        targetText.text = question;
        btn_submit.interactable = true;
        StartCoroutine("UpdateContentSize");
    }

    private IEnumerator UpdateContentSize()
    {
        MarkPosList =new ArrayList();
        MarkTimeList =  new ArrayList();
        MarkIndexList = new ArrayList();
        yield return new WaitForSeconds(1);
        textureOriginScreenPosition = Camera.main.WorldToScreenPoint(textureOrigin.position);
        Debug.Log(textureOriginScreenPosition);
        textureEndUPScreenPosition = Camera.main.WorldToScreenPoint(textureUPEnd.position);
        Debug.Log(textureEndUPScreenPosition);
        boxCollider.size = new Vector3(targetImage.GetComponent<RectTransform>().rect.width, targetImage.GetComponent<RectTransform>().rect.height, 1);
        enableMark = true;

    }

    public void submit() {
       string ansString =  SaveInfo();
       Debug.Log("ansString:"+ ansString);
        //保存到數據庫
        if (!Controller.instance.QuestionList[Controller.instance.CurrentQuestion].IsTrial)
        {
            //調整題目數量
            Controller.instance.UpdateQuestionNum();
            Controller.instance.myWWW.SaveToDatabase(Controller.instance.userId, int.Parse(Controller.instance.QuestionList[Controller.instance.CurrentQuestion].Index), ansString, Controller.instance.startTimeStamp, Controller.GetTimeStamp());

        }
        //清空數據
        int childCount = circles.transform.childCount;
        for (int i = childCount - 1; i >= 0; i--) {
            Debug.Log("childCount:"+ i);
            Destroy(circles.transform.GetChild(i).gameObject);
        }
        enableMark = false;
        btn_submit.interactable = false;
      
        Controller.instance.StartPostQuestion();
    }

    public string SaveInfo() {
        string[] ansStrings = new string[MarkIndexList.Count];
        for (int i = 0; i < MarkIndexList.Count; i++) {
            ansStrings[i] = "(" + MarkIndexList[i] + "," + MarkTimeList[i] + ")";
        }
        string ansString = string.Join(",", ansStrings);

        return ansString;
    }
}
