﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeBgColor : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleBgColor() {

        //獲得所有同輩的東西
        foreach (Transform tf in transform.parent) {
            tf.gameObject.GetComponent<Image>().color = Color.white;
        }

        gameObject.GetComponent<Image>().color = Color.cyan;

        //禁止当前所有的按键可以按

        SendMessageUpwards("ToggleEnableAns", false);
        int ansIndex = transform.GetSiblingIndex();
        StartCoroutine(SendAns(ansIndex));
       
    }

    IEnumerator SendAns(int index) {
        yield return null;
        SendMessageUpwards("SetAns", index);
    }
}
