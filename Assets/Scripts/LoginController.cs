﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoginController : MonoBehaviour
{
    public string username="";
    public string classname ="";
    public string stNum = "";
    public int stId = 0;
    public string hasFinished = "";
    public int totalSeconds = 0;

    public Text UserName;
    public Text ClassName;
    public Text StNum;


    public GameObject LoginPanel;
    public GameObject ConfirmPanel;
    public Dropdown Class_Select;
    public List<Class_Info> ClassList;
    
    //public Text Sex;
    //public Text Birth;

    public Text Hint;

    public WWWController myWWW;

    // Start is called before the first frame update
    void Start()
    {
        //StayorNot();
        myWWW = this.GetComponent<WWWController>();
        Hint.gameObject.SetActive(false);
        LoginPanel.SetActive(true);
        ConfirmPanel.SetActive(false);
        StartCoroutine(myWWW.GetClassInfo());
    }


    public void SetClassInfo(List<Class_Info> data) {
        ClassList = data;
        //綁定數據
        Class_Select.options.Clear();
        Dropdown.OptionData temoData;
        
        foreach (Class_Info classinfo in ClassList) {
            temoData = new Dropdown.OptionData();
            temoData.text = classinfo.classname;
            Class_Select.options.Add(temoData);
        }

        Class_Select.captionText.text = ClassList[0].classname;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetUserName() {
        username = UserName.text;
        Hint.gameObject.SetActive(false);
    }

    public void GetStNum()
    {
        stNum = StNum.text;
    }

    /// <summary>
    /// 判斷本地有沒有保存的用戶id
    /// 如果沒有，則在當前頁面，如果有，則直接進入到下個界面
    /// </summary>
    /// 
    public void StayorNot() {
        Debug.Log("scolarId:"+ PlayerPrefs.GetString("scolarId"));
       string playerId =  PlayerPrefs.GetString("scolarId");
        if (playerId != "") {
            SceneManager.LoadScene("Main");
        }
    }

    public void Loginin() {
        //獲取出生日期
        //GetBirth();
        

        //不存在名字
        Hint.gameObject.SetActive(true);
        
        //判斷是否為空
        if (stNum == "")
        {
            Hint.text = "所有内容都要填写";
        }
        else {
            int classid = -1;
            classname = ClassName.text;
            foreach (Class_Info classinfo in ClassList) {
                if (classinfo.classname == classname) {
                    classid = classinfo.id;
                    break;
                }
            }

            myWWW.FindName(classid, stNum);
        }

            
        //存在名字，拿到已完成的東西，並在prefeb保存下來
    }

    public void Confirm(bool iscorrect) {
        if (iscorrect)
        {
            StartCoroutine(AfterConfirm());
        }
        else {
            Hint.gameObject.SetActive(true);
            Hint.text = "请确认你的班级和学号，如有疑问请找老师帮忙。";
            ConfirmPanel.SetActive(false);
            LoginPanel.SetActive(true);
        }
    }

    private IEnumerator AfterConfirm() {
        yield return null;

        PlayerPrefs.SetInt("scolarId", stId);
        PlayerPrefs.SetString("hasFinished", hasFinished);
        PlayerPrefs.SetInt("totalSeconds", totalSeconds);
        SceneManager.LoadScene("Main");
    }

    public void FindNameSuccess(user user) {
        UserName.text = user.username;
        stId = user.id;
        hasFinished = user.hasFinished;
        totalSeconds = user.totalSeconds;
        ConfirmPanel.SetActive(true);
        LoginPanel.SetActive(false);
    }

    public void FindNamerFail()
    {
        Hint.text = "请检查你的班别、学号是否正确。如有疑问，请举手示意老师。";
    }

    public void ConnectedFail()
    {
        Hint.text = "网络有问题，请检查网络。";
    }
}
