﻿using UnityEngine;
using UnityEngine.UI;

public class CategoryController : MonoBehaviour
{
    
    public CategoryFormat Category;
    private Button thisButton;
    private bool OnclickEnabled;

    private void Awake()
    {
        thisButton = gameObject.GetComponent<Button>();
        thisButton.onClick.AddListener(() => { LoadCategory(); });
    }
    
    

    private void LoadCategory()
    {
        if (OnclickEnabled)
        {
            if (Category.Time2Finishe > Controller.instance.totalSeconds)
            {
                transform.parent.GetComponent<CategoriesParent>().PopInfo("这项任务需要的时间是" + Category.Time2Finishe/60 + "分"+ Category.Time2Finishe % 60 + "秒，距离下课时间还有" + (Controller.instance.WholeTimer.GetComponent<TimerController>().TimeLeft / 60) + "分"+ (Controller.instance.WholeTimer.GetComponent<TimerController>().TimeLeft % 60) + "。确定要继续进行测试吗？", 1);
            }
            else {
                Controller.instance.LoadCategory(Category);
            }
            
            
        }
        else {
            transform.parent.GetComponent<CategoriesParent>().PopInfo("这项测试现在不可操作，请先完成第一个测试任务",0);
        }
            
    }
    


    public void SetActive(bool active)
    {

        thisButton.transform.GetChild(2).gameObject.SetActive(!active);
        OnclickEnabled = active;
    }

    private void OnDestroy()
    {
        thisButton.onClick.RemoveAllListeners();
    }


}