﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class CategoriesParent : MonoBehaviour
{
    //[HideInInspector]
    public List<CategoryController> Categories = new List<CategoryController>();

    private LanguageList CurrentLanguage = new LanguageList();

    Dictionary<int, int> CategoryTestIndexDic = new Dictionary<int, int>();

    private string[] notshownList = new string[0];



    private void Start()
    {
        SetupCategories(true);

        AlignGameCategories();

       Controller.instance.InfoPanel.SetActive(false);
 
    }

    private void Update() {
        UpdateCategoryShow();
    }

    private void OnEnable()
    {
        LocalizationManager.onLanguageChange += SetupCategories;
    }

    private void SetupCategories(bool Force = false)
    {
      
        int Activated = 0;
        if (CurrentLanguage == LocalizationManager.instance.CurrentLanguage && !Force)
            return;

        CurrentLanguage = LocalizationManager.instance.CurrentLanguage;


        for (int x = 0; x < Categories.Count; x++)
        {
            if (Categories[x].Category.Language == CurrentLanguage)
            {
                Categories[x].gameObject.SetActive(true);
                if (Categories[x].Category.Mode == DownloadMode.Offline)
                {
                    Categories[x].Category.TestIndex = int.Parse(Regex.Replace(Categories[x].Category.OfflineFile.name, @"[^0-9]+", ""));
                }
                else
                {
                    Categories[x].Category.TestIndex = int.Parse(Regex.Replace(Categories[x].Category.OnlinePath, @"[^0-9]+", ""));
                }
                CategoryTestIndexDic.Add(Categories[x].Category.TestIndex, x);

                Activated++;
            }
            else
                Categories[x].gameObject.SetActive(false);
        }

        if (Activated == 0)
        {
            for (int x = 0; x < Categories.Count; x++) {
                Categories[x].gameObject.SetActive(true);               
            }

           
        }

        UpdateCategoryShow();

    }

    private void UpdateCategoryShow() {
        notshownList = PlayerPrefs.GetString("hasFinished").Split(',');
        if (PlayerPrefs.GetString("hasFinished") != ""  )
        {
            
            //隐藏已经完成的任务的图标
            for (int i = 0; i < notshownList.Length; i++)
            {
                if (notshownList[i] != "") {
                    Categories[CategoryTestIndexDic[int.Parse(notshownList[i])]].gameObject.SetActive(false);
                }
               
            }
        }

       

        //获得当前显现的类别
        List<CategoryController> ActiveCategories = new List<CategoryController>();
        for (int i = 0; i < Categories.Count; i++) {
            if (Categories[i].gameObject.activeSelf) {
                //让它不可动
                Categories[i].SetActive(false);
                ActiveCategories.Add(Categories[i]);
            }
        }

        //判断数量
        if (ActiveCategories.Count == 0)
        {
            Controller.instance.ErrorPanel.gameObject.SetActive(true);
            Controller.instance.ErrorPanel.GetComponentInChildren<Text>().text = "你已经完成所有的测试任务";
            Controller.instance.ErrorPanel.GetComponentInChildren<Button>().gameObject.SetActive(false);
            

        }
        else {
            Controller.instance.ErrorPanel.gameObject.SetActive(false);
            ActiveCategories[0].SetActive(true);
            //只有第一个是可以按的，其它都不可以
        }

    }

    internal CategoryController GetSingleCategory()
    {
        CurrentLanguage = LocalizationManager.instance.CurrentLanguage;

        for (int x = 0; x < Categories.Count; x++)
        {
            if (Categories[x].Category.Language == CurrentLanguage)
            {
                return Categories[x];
            }
        }

        return Categories[0];
    }

    internal void AlignGameCategories()
    {
        RectTransform Rect = gameObject.GetComponent<RectTransform>();

        if (!Rect)
            return;

        Rect.anchorMin = new Vector2(0, 1);
        Rect.anchorMax = new Vector2(1, 1);
        Rect.pivot = new Vector2(0.5f, 1);

        Rect.anchoredPosition = new Vector2(0, 0);
        Rect.sizeDelta = new Vector3(0, 0, 0);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="info"></param>
    /// <param name="type">0->通知 1->选择</param>
    internal void PopInfo(string info,int type) {
        Controller.instance.InfoPanel.SetActive(true);
        Controller.instance.InfoPanel.transform.Find("txt_Content").gameObject.GetComponent<Text>().text = info;
        switch (type) {
            case 0:
                Controller.instance.InfoPanel.transform.Find("Btn_IC").gameObject.SetActive(true);
                Controller.instance.InfoPanel.transform.Find("Btn_choose").gameObject.SetActive(false);
                break;
            case 1:
                Controller.instance.InfoPanel.transform.Find("Btn_IC").gameObject.SetActive(false);
                Controller.instance.InfoPanel.transform.Find("Btn_choose").gameObject.SetActive(true);
                break;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public void LoadCategory()
    {
        //获取第一个显示的类型
        for (int i = 0; i < Categories.Count; i++) {
            if (Categories[i].gameObject.activeSelf) {
                Controller.instance.LoadCategory(Categories[i].Category);
                return;
            }
        }

    }

    private void OnDestroy()
    {
        LocalizationManager.onLanguageChange -= SetupCategories;
    }
}